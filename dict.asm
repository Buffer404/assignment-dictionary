%include "lib.inc"

%define SZ 8

global find_word

section .text
find_word:
;Проверка указателя
    test rsi, rsi
    jz .fail

    add rsi, SZ
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
;Поиск
    test rax, rax
    jnz .success

    mov rsi, [rsi - SZ]
    jmp find_word

    .fail:
        xor rax, rax
        ret

    .success:
        mov rax, rsi
        sub rax, SZ
        ret
