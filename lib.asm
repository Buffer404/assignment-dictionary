global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global string_copy
global read_char
global read_word
global parse_uint
global parse_int

section .text


; Принимает код возврата и завершает текущий процесс
exit:
mov rax, 60 ; invoke 'exit' system call
xor rdi, rdi
syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax ;Длина строки = 0
.loop:
  cmp byte [rdi + rax], 0; Проверка байта на нуль
  je .end ;Если ZF=1 т.е. Это он, то выход
  inc rax ; Длина сторки+=1
  jmp .loop; Обратно в цикл
.end:
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
push rbx
mov rbx, rdi; Строка в rbx
call string_length
mov rdx, rax; Длина строки
mov rax, 1 ; 'write' syscall number
mov rdi, 1 ; stdout descriptor
mov rsi, rbx ; string address
syscall
pop rbx
ret

; Принимает код символа и выводит его в stdout
print_char:
push rdi
mov rdx, 1; Длина строки
mov rax, 1 ; 'write' syscall number
mov rsi, rsp ; string address
mov rdi, 1 ; stdout descriptor
syscall
pop rdi
ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1 ;system call number should be stored in rax
    mov rdi, 1 ; argument #1 in rdi: where to write (descriptor)?
    mov rsi, 10 ; argument #2 in rsi: where does the string start?
    mov rdx, 1 ; argument #3 in rdx: how many bytes to write?
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
    push r12
    push r14
    mov r14, rsp
    mov rax, rdi
    mov r12, 10 ; делитель
    dec rsp
    .loop:
        xor rdx, rdx
        div r12 ; в rax целая часть, в rdx остаток
        add rdx, 48 ;ascii
        dec rsp
        mov [rsp], dl
        test rax, rax ; проверка на ноль?
        jnz .loop
    .out:
        mov rdi, rsp
        call print_string
    mov rsp, r14
    pop r14
    pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    push rbx
    mov rbx, rdi
    cmp rbx, 0
    jns .count
    mov rdi, '-'
    call print_char
    mov rdi, rbx
    neg rdi
    .count:
      call print_uint
      pop rbx
      ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    mov rdi, [rsp]
    push rax
    call string_length
    cmp rax, [rsp]
    je .check
    add rsp, 3*8
.error:
    mov rax, 0
    ret

.check:
    pop rcx
    pop rsi
    pop rdi
.loop:
    cmp rcx, 0
    jbe .end
    mov dl, byte [rsi + rcx - 1]
    cmp dl, byte [rdi + rcx - 1]
    jne .error
    dec rcx
    jmp .loop

.end:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

push 0
mov rsi, rsp
xor rax, rax ; rax <- 0 (syscall number for 'read')
xor rdi, rdi ; edi <- 0 (stdin file descriptor)
mov rdx, 1 ; rdx <- size of the buffer
syscall
pop rax
ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi        ; address of buffer start
    push r12
    mov r12, rdi    ; callee saved registers
    push r13
    mov r13, rsi
.loop_space:
    call read_char
    cmp rax, 0x20
    je .loop_space
    cmp rax, 0x09
    je .loop_space
    cmp rax, 0xA
    je .loop_space
.loop:
    cmp rax, 0x0
    je .finally
    cmp rax, 0x20
    je .finally
    cmp rax, 0x9
    je .finally
    cmp rax, 0xA
    je .finally
    dec r13        ; will be if size=n n+1 cuz of null-terminator
    cmp r13, 0
    jbe .of
    mov byte [r12], al
    inc r12
    call read_char
    jmp .loop

.finally:
    mov byte [r12], 0   ; putting null terminator
    pop r13             ; pop r13
    pop r12             ; pop r12
    mov rdi, [rsp]      ; get rdi from rsp to add func args
    call string_length
    mov rdx, rax        ; put size in rdx
    pop rax             ; put rdi in rax
    ret

.of
    pop r13             ; get back everything from stack
    pop r12
    pop rdi
    mov rax, 0
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rsi, 0x0A
    xor r8, r8          ; результат
    xor rax, rax
    xor rcx, rcx        ; длина
    .loop: ;считка в цикле
    mov al, byte[rdi + rcx]
    cmp al, '0'
    jb .end ;меньше 0
    cmp al, '9'
    ja .end ; больше 9
    sub al, '0'
    push rax
    mov rax, r8
    mul rsi
    mov r8, rax
    pop rax

    add r8, rax
    inc rcx
    jmp .loop

    .end:
    mov rdx, rcx
    mov rax, r8
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte [rdi]
    inc rdi ;проверка на знак
    cmp al, '-'
    je .min
    cmp al, '+'
    je .pls
    dec rdi
.pls:
    call parse_uint
    ret

.min:
    call parse_uint
    neg rax
    inc rdx         ; symbol minus
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    call string_length  ; return rax
    cmp rdx, rax
    jb .false
    mov rcx, rax
.start:
    test rcx, rcx
    je .true
    dec rcx                 ; idk should i print string termination symbol
    mov dl, byte [rdi + rcx]   ; put string in buffer
    mov byte [rsi + rcx], dl
    jmp .start
.true:
    mov byte [rsi + rax], 0
    ret
.false:
    mov rax, 0
    ret
